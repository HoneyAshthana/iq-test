from flask import Flask, jsonify, request
from flask_restful import Resource,Api
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from config import iq_test_db

from AdminAction.uploadQuestions import UploadQuestion
from Users.addAdmin import Admin
from Login.login import Login
from Users.uploadUsers import UploadUsers
from Users.addUsers import AddUsers
from Users.delUser import DelUser

from Users.addSuperAdmin import AddSuperAdmin

# """Cython"""
# import pyximport; pyximport.install()

# from AdminAction.assignTest import AssignTest
# from AdminAction.addTemplate import AddTemplate
from AdminAction.addSet import AddSet
# from AdminAction.editSet import EditSet

# from AdminAction.getTestPaper import GetTestPaper
from AdminAction.userDetails import GetUserInfo
from AdminAction.getSet import GetSet
from AdminAction.submitTest import SubmitResult
from AdminAction.password_reset import ChangePassword
app = Flask(__name__)
bcrypt = Bcrypt(app)
api = Api(app)

CORS(app,support_credentials=True)

app.config["CORS_HEADERS"]='Authorization'

api.add_resource(Login,"/iqtest/login")
api.add_resource(Admin, "/iqtest/addAdmin")

api.add_resource(UploadQuestion, "/iqtest/uploadQtn")
api.add_resource(UploadUsers, "/iqtest/uploadUsers")
api.add_resource(AddUsers, "/iqtest/addUsers")
api.add_resource(DelUser, "/iqtest/delUser")

api.add_resource(AddSet, "/iqtest/addSet")
# api.add_resource(EditSet, "/iqtest/editSet")
api.add_resource(GetUserInfo, "/iqtest/getUserInfo")
api.add_resource(GetSet, "/iqtest/getSet")
api.add_resource(SubmitResult, "/iqtest/result")
api.add_resource(AddSuperAdmin, "/iqtest/addSuperAdmin")

api.add_resource(ChangePassword, "/iqtest/changePassword")

# api.add_resource(AddTemplate, "/iqtest/addTemplate")
# api.add_resource(AssignTest, "/iqtest/assignTest")

# api.add_resource(GetTestPaper,'/iqtest/setId=<string:set_id>&userId=<string:user_id>',endpoint="template, users")
# api.add_resource(GetTestPaper,'/iqtest/userId=<string:user_id>&setId=<string:set_id>',endpoint="users, template")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port = 5000, debug=True)