from config import iq_test_db, jwt_secret
from flask_restful import Resource
from flask import request, jsonify
import jwt
from flask_cors import cross_origin
from flask_bcrypt import Bcrypt
import datetime

class Login(Resource):
    """Admin Login in which token expires in 1 day
        Args: 
            email_id
            password
    """
    
    @cross_origin()
    def post(self):
        
        try:
            data=request.get_json(force = True)
            # print(data)
        
        except Exception as e:
            return jsonify({"success":False, "error":e.__str__()})

        try:
            email_id = data["email_id"]
            password = data["password"]            
            bcrypt = Bcrypt(None)
            admin_exists = iq_test_db.users.find_one({'email_id': email_id}, {"_id":0})     
            # print(admin)         
            if admin_exists is not None:
                if admin_exists['role'] == 'admin' or admin_exists['role'] == 'super_admin': 
                    password = bcrypt.check_password_hash(admin_exists['password'], password)   
                    if password:
                        token_json = { "admin_id": admin_exists["admin_id"],
                            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=0)}
                        token =  jwt.encode(token_json, jwt_secret, algorithm="HS256")
                        # print(token)
                        return jsonify({"success":True, "token": str(token.decode("utf-8")), "email_id": email_id, 'message':'login successfull', 'role':admin_exists['role']})                  
                    else:
                       
                        return jsonify({"success":False, "message": "Incorrect password"})
                else:
                    return jsonify({'success': False, 'message': 'Role not matched'})
            else:
                return jsonify({"success":False, "message": "You are not registerd"})     
        except Exception as e:
            return jsonify({"success":False,"error":e.__str__()})
             