import requests
import json
import uuid
from faker import Faker
f = Faker()

import time
from datetime import datetime
from pytz import timezone
TIME_ZONE =  'Asia/Kolkata'
server = "http://0.0.0.0:5000/"

def itime():
    india  = timezone(TIME_ZONE)
    now = datetime.now(india)
    tt = datetime.timetuple(now)
    n_time = time.mktime(tt)
    return n_time
def addadmin(email_id,password):
    data = {
        "email_id":email_id,
        "password" : password,
        }

    r = requests.post(server+  "/iqtest/addAdmin",data = data)
    if r.json()["success"]:
        print (email_id , " admin added\n")
        return r.json()["user_id"]
    else:
        print(r.json()["message"])
        if "error" in r.json():
            print(r.json()["error"])
    return None
def login(email_id,password):
    data = {
        "email_id" : email_id,
        "pwd" : password
        }
    r = requests.post(server+  "/iqtest/login/",data = data,verify=False)
    if r.json()["success"]:
        print (email_id , " Logged in succesfully\n")
        return r.json()
    else:
        print(r.json()["message"])
        if "error" in r.json():
            print(r.json()["error"])
    return None
def test_addadmin(admin_email,data):
    temp = addadmin("honey.ashthana1@gmail.com","12345")
    print (temp)
    if temp:
        data[admin_email] = temp
        # save(data)
        print (data)

# addadmin(f.email(),'12345')