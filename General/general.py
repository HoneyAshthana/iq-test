# Import smtplib for the actual sending function
import smtplib
# Import email modules
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from config import sender_email,sender_password

def send_email(toaddr, ccaddr, subject, body):
    """Send email"""
    fromaddr = sender_email
    server = smtplib.SMTP('smtp.gmail.com: 587')
    text='Hey u'
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject
    # print(toaddr)
    # print(subject)
    # print(body)
    if ccaddr is not None:
        rcpt = ccaddr + [toaddr]
        msg['Cc'] = ", ".join(ccaddr)
    else:
        rcpt = toaddr
    print(rcpt)
    # a = url
    # print(type(url))
    # print(url)
    html = """\
    <html>
      <font face="arial" size="2"> Hello
      </font><br />
      <br>
      <font face="arial" size="2"> {body}
      </font><br />
      
      <font face="arial" size="2">Regards
      </font>
      <br>  
      <font face="arial" size="2">QCI LMS/IT-Cell
      </font>
      <br>
      <img src="http://qcin.org/images/qci-logo.jpg" width="150" height="40" />
    </html>""".format(body=body)

    msg.attach(MIMEText(html, 'html'))
    print(text)
    server.starttls()
    server.login(sender_email, sender_password)
    text = msg.as_string()
    server.sendmail(fromaddr, rcpt, text)
    server.quit()


# encrypt and decrypt url
import urllib.parse
from cryptography.fernet import Fernet
from config import key
def encrypt_data(message):                                                                                            
    f = Fernet(key)                                                                                                       
    encrypted = f.encrypt(message.encode('utf-8'))   
    return encrypted


def decrypt_data(id):
    f = Fernet(key) 
    encrypted =bytes(id, 'utf-8')
    # print(encrypted)
    decrypted = f.decrypt(encrypted)
    decrypted_data = decrypted.decode("utf-8")
    # print(encrypted)
    return decrypted_data

import time
from datetime import datetime
from pytz import timezone
TIME_ZONE =  'Asia/Kolkata'

def itime():
    """Conversion from date to epoch value
        Args:
        date : date of which epoch value need to be calculated
    """
    india  = timezone(TIME_ZONE)
    now = datetime.now(india)
    tt = datetime.timetuple(now)
    n_time = time.mktime(tt)
    return n_time

"""method creates csv from user data"""
#not used
import os
import csv
from io import StringIO
def create_csv(emp_data):
    # open a file for writing
    employ_data = open('EmployData.csv', 'w')

    # create the csv writer object
    csvwriter = csv.writer(employ_data)
    count = 0
    for emp in emp_data:
        if count == 0:
            header = emp.keys()
            csvwriter.writerow(header)
            count += 1
        csvwriter.writerow(emp.values())
    employ_data.close()
    
    # url = os.path.abspath("EmployData.csv") 
    # print(url)
    # encoding csv file
    myfile = open('EmployData.csv', 'r')
    stream = StringIO(myfile.read())
    encoded = base64.b64encode(stream.getvalue().encode('utf-8'))
    myfile.close()
    return encoded

import base64
"""fn returns csv data and in bytes that is base 64 encoded"""
def encodedCsvData(data):
    csv_data = []
    count =0 
    for i in data: 
        if count == 0: 
            headers = list(i.keys())
            # print(headers)
            csv_data.append(headers) 
            count +=1 
        csv_data.append(list(i.values()))
    # print(csv_data)
    f = StringIO()
    csv.writer(f).writerows(csv_data)
    encoded_csv_data = base64.b64encode(f.getvalue().encode())
    encoded_csv_data = encoded_csv_data.decode('utf-8')
   
    return encoded_csv_data