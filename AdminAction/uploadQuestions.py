# import boto3
from flask import request,jsonify
from flask_restful import Resource
from Decorators.auth import auth
from flask_cors import cross_origin
from config import iq_test_db
import openpyxl
from io import BytesIO
# from config import aws_access_key_id, aws_secret_access_key, bucket_name
import uuid

"""Creates connection with aws and uploads file and then stores it in mongodb"""
def uploadQuestions(decoded_hash, file=None):
    
    # s3Client = boto3.client('s3',aws_access_key_id = aws_access_key_id, aws_secret_access_key = aws_secret_access_key)
    #uploading file to s3
    # s3Client.put_object(Bucket=bucket_name, Key='QuestionTemplate.xlsx', Body=file)

    #fetching file from s3
    # obj= s3Client.get_object(Bucket=bucket_name, Key='QuestionTemplate.xlsx')
    # binary_data = obj['Body'].read()
    list_hash = iq_test_db.question_upload.distinct('q_hash')
    wb = openpyxl.load_workbook(BytesIO(decoded_hash))
    sheet = wb.active
    rows = sheet.rows
    first_row = [cell.value for cell in next(rows)]
    first_row = [x for x in first_row if x is not None]
    duplicate_qn = []
    for row in rows:
        record = {}
        for key, cell in zip(first_row, row):
            if key == 'Answer':
                key = 'ans'
            elif key == 'Option A':
                key = 'option_a'
            elif key == 'Option B':
                key = 'option_b'   
            elif key == 'Option C':
                key = 'option_c'
            elif key == 'Option D':
                key = 'option_d'
            elif key == 'Option E':
                key = 'option_e'
            elif key == 'Difficulty Level':
                key = 'difficulty_level'
            elif key == 'Section':
                key = 'section'
            elif key == 'Marks':
                if type(cell.value) == int:
                    key = 'marks'
                else:
                    info = "Upload unsuccessfull as marks is not integer"            
            elif key == 'Question':
                key = 'question'
                qn_hash = hash(cell.value.strip())
                record['q_hash'] = qn_hash
            elif key == 'Image URL':
                key = 'image_url'
            
            if cell.data_type == 's' and key != 'None':
                record[str(key)] = cell.value.strip()
            else:
                record[str(key)] = str(cell.value)
        record['question_id'] = uuid.uuid4().hex
        # print(record)
        if qn_hash not in list_hash:
            iq_test_db.question_upload.insert(record)
        else:
            pass
import base64
# from io import BytesIO
class UploadQuestion(Resource):
    @auth
    @cross_origin()
    def post(self):
        """to upload question bank
            Args: 
                file:
        """
        try:
            file = request.json['file'].split(',')
            # print(file[0])
            decoded_hash = base64.b64decode(file[1])

        except Exception as e:
            return jsonify({"message": "Upload unsuccessfull", "error":e.__str__(), "success": False})
        
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                uploadQuestions(decoded_hash)
                return jsonify({"message": "File uploaded successfull", "success": True})
            else:
                return jsonify({"message": "U are not admin", "error":e.__str__(), "success": False })
        except Exception as e:
            return jsonify({"message": "Upload unsuccessfull", "error":e.__str__(), "success": False})


    @auth
    @cross_origin()
    def get(self):
        """to get all questions from question bank"""
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                result = list(iq_test_db.question_upload.find({}, {'_id':0, 'q_hash':0}))
                # print(result)
            else:
                return jsonify({"message": "U are not admin", "error":e.__str__(), "success": False})
            return jsonify({ "qtn_bank": result, "success": True})

        except Exception as e:
            return jsonify({"message": "No data found", "error":e.__str__(), "success": False})