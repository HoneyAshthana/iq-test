from flask_restful import Resource
from config import iq_test_db
from flask import request, jsonify
# from Decorators.auth import auth
from flask_cors import cross_origin 
from General.general import decrypt_data, itime
    
class GetSet(Resource):      
    # @auth
    @cross_origin()
    def post(self):
        """ Returns the particular test paper 
            Args :
                token :
                user_id:
        """
        try:
            data = request.get_json(force=True)
            # print(data)
            token = data['token']
            user_id = data['user_id']
            
        except Exception as e :
            return jsonify({'success' : False, 'error' : e.__str__()})
        try:

            set_id = decrypt_data(token)
            # print(set_id)
            set_data = iq_test_db.template.find_one({'set_id':set_id},{"_id":0})
            user_exists = iq_test_db.users.find_one({'user_id': user_id},{'_id':0})
            if user_exists['set_id'] == set_id :
                """disabled check"""
                user_exists['test_undergone'] = False
                if user_exists['test_undergone'] == False and user_exists['marks'] == "":
                    question_set_data = []
                    if set_data['question_array'] is not None:
            
                        for i in set_data['question_array']:
                            el = iq_test_db.question_upload.find_one({'question_id': i}, {'_id':0,'question_id':0})
                            option = el['option_a'],el['option_b'],el['option_c'],el['option_d'],el['option_e']
                            print(option)
                            el['options'] = list(filter('None'.__ne__, option))                        
                            del el['option_a'], el['option_b'], el['option_c'], el ['option_d'], el['option_e']
                            if el['image_url'] == 'None':
                                del el['image_url']
                            question_set_data.append(el)
                        iq_test_db.users.update({'user_id':user_id},{'$set':{'test_undergone':True,'start_time':itime(),'status':'Appeared'}})
                        del set_data['question_array']
                        return jsonify({"success" : True, "template_data" : question_set_data, 'set_data':set_data}) 
                    else:
                        return jsonify({"success" : False, "messages" : "Test paper currently not available"})
                else:
                    return jsonify({"success" : False, "message" :'Already Test undergone'})
            else:
                return jsonify({"success" : False, "message" :'token and set id does not match'})

        except Exception as e:
            return jsonify({'success' : False, 'error' : e.__str__()})