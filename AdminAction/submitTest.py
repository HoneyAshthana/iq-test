from flask_restful import Resource
from config import iq_test_db
from flask import request, jsonify
# from Decorators.auth import auth
from flask_cors import cross_origin 
from General.general import decrypt_data, itime
# from config import basic_url
    
class SubmitResult(Resource):      
    # @auth
    @cross_origin()
    def post(self):
        """ submit the particular test paper 
            Args :
                test_no :
        """
        try:
            data = request.get_json(force=True)
            print(data)
            token = data['token']
            user_id = data['user_id']
            total_marks_obtained = data['total_marks_obtained']
            time_stamp = data['time_stamp']
            # status = data['status']
            answered_data = data['_list']
        except Exception as e :
            return jsonify({'success' : False, 'error' : e.__str__()})
        try:
            
            set_id = decrypt_data(token)
            print(set_id)
            set_data = iq_test_db.template.find_one({'set_id':set_id},{"_id":0})
            print(set_data)
            end_time = itime()
            user_exists = iq_test_db.users.find_one({'user_id': user_id},{'_id':0})
            print(user_exists)
            if user_exists['set_id'] == set_id and user_exists['test_undergone'] == True and user_exists['marks'] == '':
                
                # print(end_time)
                # print(user_exists['start_time'])
                if (end_time - user_exists['start_time']) <= (float(set_data['time'])*60.0):
                   
                    data={'user_id':user_id,
                                                'total_marks_obtained':total_marks_obtained,
                                                'time_stamp':time_stamp,
                                                'answered_data': answered_data,
                                                'status':'Appeared'
                                        }
                    iq_test_db.user_log.insert(data)
                    # print(user_id)
                    iq_test_db.users.update({'user_id':user_id},{'$set':{'marks': total_marks_obtained,'time_stamp': time_stamp, 'end_time': end_time}})
                    return jsonify({"success" : True, "total_marks_obtained":total_marks_obtained, 'message':'Test validated under correct time duration', 'user_id':user_id}) 
                else:
                    return jsonify({"success" : False, 'message':'More time taken than given duration'}) 
            else:
                return jsonify({"success" : True, "message" : 'Requesting wrong set'})                
            

        except Exception as e:
            return jsonify({'success' : False, 'error' : e.__str__()})