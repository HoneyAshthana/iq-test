from flask import jsonify,request
from flask_restful import Resource
from config import iq_test_db
from flask_cors import cross_origin
from Decorators.auth import auth
# from Decorators.roles_required import roles_required
"""Not used"""
class AddSet(Resource) :
    @auth
    @cross_origin()
    def post(self) :
        """Add new exam set manually if set id does not exist and if set exists it edits the set
        Args:
            set_id : set_id
            name : set name
            time :
            total_marks:
            question_array : array of question id in that set
        """
        try :
            data = request.get_json(force=True)
            # print(data)
            set_id = data['set_id']
            name = data['name']
            time = data['time']
            total_marks = data['total_marks']
            question_array = data['question_array']
            instruction = data['instruction']
        except Exception as e:
            return jsonify({"success": False, "error": e.__str__()})
        
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'super_admin':
                set_exists = iq_test_db.template.find_one({'set_id':set_id},{"set_id":1})
                print(set_exists)
                if set_exists:
                    iq_test_db.template.update({'set_id':set_id},
                                {                      
                                'name' : name,
                                'time' : time,
                                'total_marks': total_marks,
                                'question_array': list(question_array),
                                'set_id': set_id,
                                'instruction': instruction
                                },upsert = True)
                    return jsonify({"success" : True, "message" : "set edited successfully"})
                else:
                    new_set = {                         
                                'set_id' : set_id,
                                'name' : name,
                                'time' : time,
                                'total_marks': total_marks,
                                'question_array': list(question_array),
                                'instruction': instruction 
                                }
                    # print(new_set)       
                    iq_test_db.template.insert_one(new_set)
                    return jsonify({"success" : True, "message" : "New set added successfully"})
            else:
                return jsonify({"success" : False, "message" : "You cannot add set/Inappropriate role"})

        except Exception as e :
            return jsonify({"success" : False, "error" : e.__str__()}) 


    @auth
    @cross_origin()
    def get(self):
        """function to get all sets crearted"""
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                result = list(iq_test_db.template.find({}, {"_id": 0,}))
                return jsonify({ "exam_set": result, "success": True})
            else:
                return jsonify({"success" : False, "message" : "U are not admin", "error":e.__str__()})
        except Exception as e:
            return jsonify({"message": "Error in displaying set", "error":e.__str__(), "success": False})