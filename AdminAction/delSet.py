from flask import jsonify,request
from flask_restful import Resource
from config import iq_test_db
from flask_cors import cross_origin
from Decorators.auth import auth

class DelSet(Resource) :
    @auth
    @cross_origin()
    def post(self) :
        """Del exam set manually
        Args:
            set_id : set_id
        """
        try :
            data = request.get_json(force=True)
            print(data)
        except Exception as e:
            return jsonify({"success": False, "error": e.__str__()})
        
        try:
            set_id = data['set_id']

            valid_user = iq_test_db.users.find_one({'role': 'admin'}, {"_id":0, 'email_id':0})
            if valid_user['role'] == 'admin':
                set_exists = iq_test_db.users.find_one({"set_id":set_id},{'_id':0})
                if set_exists:
                    iq_test_db.template.update({'set_id':set_id},
                                {                      
                                'set_deleted':True
                                },upsert = True)
                return jsonify({"success" : True, "message" : "set edited successfully"})
            else:
                return jsonify({"success" : False, "message" : "U are not admin"})

        except Exception as e :
            return jsonify({"success" : False, "error" : e.__str__()}) 
