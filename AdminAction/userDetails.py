from flask import jsonify,request
from flask_restful import Resource
from config import iq_test_db, url, key
from flask_cors import cross_origin
# from Decorators.auth import auth
import uuid
from General.general import send_email, encrypt_data, decrypt_data
import urllib.parse

# from cryptography.fernet import Fernet

class GetUserInfo(Resource) :
    # @auth
    @cross_origin()
    def post(self) :
        """gives user info who needs to appear for test
        Args:
            token_id: token for verification of user
        """
        try :
            data = request.get_json(force=True)
            # print(data)
            token_id = data['token_id']

        except Exception as e :
            return jsonify({'success' : False, 'error' : e.__str__()})
        
        try:
            decrypted_user = decrypt_data(token_id)
            # print(decrypted_user)
            user_details  = iq_test_db.users.find_one({'user_id': decrypted_user, 'user_deleted': False}, {'_id': 0, 'role': 0, 'user_deleted': 0})
            print(user_details)
            # set_time = iq_test_db.template.find_one({'set_id': user_details['set_']})
            if user_details:
                set_id = user_details['set_id']
                # instruction = 
                set_details = iq_test_db.template.find_one({'set_id': set_id},{'time':1, 'instruction': 1})
                time = set_details['time']
                instruction = set_details['instruction']
                """disable test undergone by uncommenting the line below"""
                user_details['test_undergone'] = False
                if user_details['test_undergone'] == False:
                    token = encrypt_data(set_id)
                    token = token.decode('utf-8')
                    # print(set_time)
                    user_details['time']=time
                    user_details['instruction'] = instruction
                    return jsonify({"success" : True, 'user_details': user_details ,'token': token})
                else:
                    return jsonify({"success" : True, 'message':'Already test undergone'})
               
            else:
                return jsonify({"success" : False, 'message': 'user no longer exists'})
        except Exception as e :
            return jsonify({"success" : False, "error": e.__str__()}) 
            