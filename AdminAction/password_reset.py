from flask import jsonify,request
from flask_restful import Resource
from config import iq_test_db
from flask_cors import cross_origin
from Decorators.auth import auth
from flask_bcrypt import Bcrypt
bcrypt =Bcrypt(None)
class ChangePassword(Resource):
    
    @auth
    @cross_origin()
    def post(self):
        """Change password of admin
            Args:
                current_password
                new_password
        """
        try:
            data = request.get_json(force = True)
            print(data)
            current_password = data['oldPassword'] 
            new_password = data['newPassword']

        except Exception as e:
            return jsonify({"success": False, "error": e.__str__()})

        try:
            print("1")
            u_id =  self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id':u_id},{'role':1,'password': 1})
            print(authenticate_admin)
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                # password_exist = iq_test_db.users
                print("2")
                print(current_password)
                match_password = bcrypt.check_password_hash(authenticate_admin['password'], current_password)  
                print("4")
                print(match_password)
                if match_password  == True:
                    print("3")
                    iq_test_db.users.update({'admin_id': u_id}, { '$set': { 'password': bcrypt.generate_password_hash(new_password)} })
                    print("5")
                    
                    return jsonify({'message':'Password changed successfully', 'success': True})
                elif match_password  == False:
                    return jsonify({'message': "please enter correct credentials for setting new password", 'success': False})
            else:
                return jsonify({'message': 'Inauthentic user/Role not suitable', 'success': False})
        except Exception as e:
            return jsonify({'success': False, 'error': e.__str__ ()})

        