from flask import request,jsonify
from functools import wraps
import jwt
from config import jwt_secret
"""
def auth(func):
		@wraps(func) # to preserve name, docstring, etc.
		# 
		def decorated(self, *args, **kwargs):
			#print(request.headers)
			try:
				token = self.request.headers["Authorization"]
				# print(token)
			except Exception as e:
				return jsonify({"success": False, "response" : 'Headers required'})
			try:
				# print(token)
				print(token)
				# _id = jwt.decode(token, jwt_secret, algorithms=["HS256"])["_id"]
				_id = jwt.decode(token, jwt_secret, algorithms=["HS256"])

				print(_id)
				return _id
			except Exception as e:
				return jsonify({"success":False, "response" : 'Wrong Token'})

			
			return func( *args, **kwargs)
		return decorated
"""
def auth(func):
	@wraps(func) # to preserve name, docstring, etc.
	def decorated(self, *args, **kwargs):
		# print ("Request headers ==%s"%request.headers)
		# print (request)
		try:

			token = request.headers["Authorization"]
			# print(token)
			token_json = jwt.decode(token, jwt_secret, algorithms=['HS256'])
			# print("From auh token %s"%token_json)
			if "admin_id" in token_json:
				self.r_id = token_json['admin_id']
				
			
		except Exception as e:
			print ("This is the error in auth %s"%e.__str__())
			return jsonify({"success":False, "response" : e.__str__()})	
		
		return func(self, *args, **kwargs)
	return decorated
