from flask_restful import Resource
from config import iq_test_db
from flask import request, jsonify
import uuid
from flask_cors import cross_origin 
from flask_bcrypt import Bcrypt

class AddSuperAdmin(Resource) :
    """Addssuper admin in the pool
    Args:
        email_id : Email of admin
        password : Admin's password
    """
    @cross_origin()
    def post(self):

        try:    
            data = request.get_json(force = True)
            # print(data)
            
        except Exception as e:
            return jsonify({"success": False, "error": e.__str__()})

        try:
            email_id = data["email_id"]
            password = data["password"]
            bcrypt = Bcrypt(None)
            super_admin_exist = iq_test_db.users.find_one({"email_id": email_id, "role": 'super_admin'}, {"_id":0, "password":0})       
            # print(admin_exist)
            if super_admin_exist:
                return jsonify({"success":False, "message": "Super Admin already exists!!"})      
            else:
                uid = uuid.uuid4().hex
                password_enc = bcrypt.generate_password_hash(password)
                new_super_admin={       
                    "email_id": email_id,
                    "password": password_enc,
                    "admin_id": uid,
                    "role" : "super_admin",
                }
                iq_test_db.users.insert_one(new_super_admin)            
                return jsonify({"success":True, "message":"Super Admin added successfully!"})
        
        except Exception as e:
            return jsonify({"success":False,"error":e.__str__()})
