from flask import jsonify,request
from flask_restful import Resource
from config import iq_test_db
from flask_cors import cross_origin
from Decorators.auth import auth
from flask_bcrypt import Bcrypt
import uuid
from config import url
from General.general import send_email, encrypt_data
import urllib.parse
class AddUsers(Resource) :

    @auth
    @cross_origin()
    def post(self) :
        """Add new user manually
        Args:
            name : name of user
            email : Email Id of user
            status:
            marks:
            time_stamp:
            set_id:
        """
        try :
            data = request.get_json(force=True)
            # print(data)
            name = data['name']
            email = data['email']
            phone = data['phone']
            status = data['status']
            marks = data['marks']
            time_stamp = data['timeStamp']
            set_id = data['set_id']

        except Exception as e :
            return jsonify({'success' : False, 'error' : e.__str__()})
        
        try:
            
            admin_id = self.r_id            
            authenticate_admin = iq_test_db.users.find_one({'admin_id':admin_id}, {"_id":0, 'email_id':0, 'password':0, 'admin_id':0})
            # print(authenticate_admin)
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                user_exists = iq_test_db.users.find_one({'email': email,'role': 'user'}, {'user_id':1})
                # print(user_exists)
                if user_exists:
                    u_id = user_exists['user_id']
                else:
                    u_id = uuid.uuid4().hex
                # print(u_id)
                if set_id is not None and email != None:
                    set_exists = iq_test_db.template.find_one({'set_id': set_id},{'set_id': 0})
                    # print(set_exists)
                    if set_exists:
                        enc = encrypt_data(u_id)
                        enc_url = url + urllib.parse.urlencode({'id': enc})
                        set_message = 'Created set assigned'
                        send_email(email, None, "IQ Test Assigned by QCI",
                                ("You need to appear for IQ Test.\
                                Please click the below link for undergoing test <br><a href = " + enc_url+ ">Click Here</a>"))
                    else:
                        set_message = 'set not created yet'
                else:
                    set_message = 'set not assigned'
                if user_exists:
                    iq_test_db.users.update({'email': email},
                                {                      
                                'name' : name,
                                'email' : email,
                                'phone' : phone,
                                'role': 'user',
                                'status' : status,
                                'marks': marks,
                                'time_stamp' :time_stamp,
                                'user_id':u_id,
                                'user_deleted': False,
                                'set_id': set_id,
                                'test_undergone':False,
                                },upsert = True)
                    return jsonify({"success" : True, "message" : "user edited successfully", 'user_id':u_id, 'info': set_message})
                else:
                    new_emp = {                      
                                'name' : name,
                                'email' : email,
                                'phone' : phone,
                                'role': 'user',
                                'status' : status,
                                'marks': marks,
                                'time_stamp' :time_stamp,
                                'user_id':u_id,
                                'test_undergone':False,
                                'user_deleted': False,
                                'set_id': set_id
                                }
                    # print(new_emp)       
                    iq_test_db.users.insert_one(new_emp)
                    return jsonify({"success" : True, "message" : "New Employee added successfully", 'user_id':u_id, 'info':set_message})
            else:
                return jsonify({"success" : False, "message" : "U are not admin"})

        except Exception as e :
            return jsonify({"success" : False, "error" : e.__str__()}) 
            