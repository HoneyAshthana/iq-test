from flask import jsonify,request
from flask_restful import Resource
from config import iq_test_db
from flask_cors import cross_origin
from Decorators.auth import auth

class DelUser(Resource) :
    @auth
    @cross_origin()
    def post(self) :
        """Del user manually
        Args:
            user_id : 
        """
        try :
            data = request.get_json(force=True)
            # print(data)
            user_id = data['user_id']
        except Exception as e:
            return jsonify({"success": False, "error": e.__str__()})
        
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'admin'or authenticate_admin['role'] == 'super_admin':
                user_exists = iq_test_db.users.find_one({"user_id": user_id},{'user_deleted':1})
                # print(user_exists)
                if user_exists['user_deleted'] == False:
                    iq_test_db.users.update({'user_id':user_id},
                                {      '$set':{               
                                'user_deleted':True
                                }
                                })
                    return jsonify({"success" : True, "message" : "user deleted successfully"})
                else:
                    return jsonify({"success" : False, "message" : "user to be deleted doesn't exists/user already deleted", 'user_id': user_id})
            else:
                return jsonify({"success" : False, "message" : "U are not admin"})

        except Exception as e :
            return jsonify({"success" : False, "error" : e.__str__()}) 
