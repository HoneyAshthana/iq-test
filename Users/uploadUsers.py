import boto3
from flask import request,jsonify
from flask_restful import Resource
from Decorators.auth import auth
from flask_cors import cross_origin
from config import iq_test_db
import openpyxl
from io import BytesIO
from flask_bcrypt import Bcrypt
# from config import aws_access_key_id, aws_secret_access_key, bucket_name
import uuid
import base64

from General.general import encodedCsvData

"""Creates connection with aws and uploads file and then stores it in mongodb"""
def uploadUsers(binary_data, file=None):
    
    # s3Client = boto3.client('s3',aws_access_key_id = aws_access_key_id, aws_secret_access_key = aws_secret_access_key)
    # #uploading file to s3
    # s3Client.put_object(Bucket=bucket_name, Key='UserTemplate.xlsx', Body=file)
    
    # #fetching file from s3
    # obj= s3Client.get_object(Bucket=bucket_name, Key='UserTemplate.xlsx')
    # binary_data = obj['Body'].read()
    distinct_emails = iq_test_db.users.distinct('email')
    wb = openpyxl.load_workbook(BytesIO(binary_data))
    sheet = wb.active
    rows = sheet.rows
    first_row = [cell.value for cell in next(rows)]
    for row in rows:
        record = {}
        for key, cell in zip(first_row, row):
            if key == 'Name':
                key = 'name'
            elif key == 'Email ID':
                key = 'email'
            elif key == 'Phone No':
                key = 'phone'
            else:
                pass
            if cell.data_type == 's':
                record[str(key)] = cell.value.strip()
            else:
                record[str(key)] = str(cell.value)
        record['role'] = 'user'
        record['user_id'] = uuid.uuid4().hex
        record['status'] = ""
        record['marks'] = ""
        record['time_stamp'] = ""
        record['user_deleted'] = False
        record['test_undergone'] = False
        if record['email'] is not None:
            if record['email'] not in distinct_emails:
                iq_test_db.users.insert(record)
            else:
                pass
            
        else:
            pass
class UploadUsers(Resource):
    @auth
    @cross_origin()
    def post(self):
        """upload list of users present on excel
            Args:
                file
        """
        try:
            file = request.json['file'].split(',')
            decoded_hash = base64.b64decode(file[1])

        except Exception as e:
            return jsonify({"success": False,"message":"error in file request", "error": e.__str__()})
        
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                uploadUsers(decoded_hash)
            
                return jsonify({"message": "File uploaded successfully", "success": True})
            else:
                return jsonify({"message": "U are not admin", "error":e.__str__(), "success": False})

        except Exception as e:
            return jsonify({"message": "Upload unsuccessfull", "error":e.__str__(), "success": False})


    @auth
    @cross_origin()
    def get(self):
        """gets all list of users in database"""
        try:
            admin_id = self.r_id
            authenticate_admin = iq_test_db.users.find_one({'admin_id': admin_id}, {"role":1})
            if authenticate_admin['role'] == 'admin' or authenticate_admin['role'] == 'super_admin':
                result = list(iq_test_db.users.find({'role':'user','user_deleted':False}, {"_id": 0, 'user_deleted':0, 'test_undergone':0, 'role':0,'start_time':0,'end_time':0}))
                # url = create_csv(result)
                encoded_data = encodedCsvData(result)
                return jsonify({ "user_data": result, "success": True, 'encoded_csv_data': encoded_data })
            else:
                return jsonify({"message": "U are not admin", "error":e.__str__(), "success": False})
        except Exception as e:
            return jsonify({"message": "Upload unsuccessfull", "error":e.__str__(), "success": False})