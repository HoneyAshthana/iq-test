"""Setting up database connection using flask PyMongo"""
from secrets import mongo_ip, mongo_port, JWT_secret
# , S3_BUCKET, S3_KEY, S3_SECRET_ACCESS_KEY
from flask_pymongo import MongoClient
mongo_ip = mongo_ip
mongo_port = mongo_port
uri  = "mongodb://" + mongo_ip + ":" + str(mongo_port)+"/"
mongo = MongoClient(uri)
iq_test_db = mongo['iqtest']

# test db
# iq_test_db = mongo['iqtest_testing']

#jwt secret key
jwt_secret = JWT_secret

#AWS Credentials
# from secrets import  S3_SECRET_ACCESS_KEY, S3_KEY,S3_BUCKET
# aws_access_key_id = S3_KEY
# aws_secret_access_key = S3_SECRET_ACCESS_KEY
# bucket_name = S3_BUCKET

# local url
url = "http://127.0.0.1:5000/#/exam/IntroPage/" 
url = "http://192.168.15.237:3000/#/exam/IntroPage/"
# url = "http://13.127.50.37/#/exam/IntroPage/"

# credentials for sending emails
from secrets import sender_email, sender_password
sender_email = sender_email
sender_password = sender_password


from cryptography.fernet import Fernet
# key = Fernet.generate_key()                                                                                               
# f = Fernet(key) 
# print(f)
key = b'VDommdArRf1_1BxFDwEvsRfljcVMSGqjWy4Ooumxx1c='